var q = require('q');

var fs = require('fs');
var util = require('util');



var base_path = fs.realpathSync(__dirname + '/../data');

var getName = function(name){

	return name.replace(/^[0-9]+_/, "");

}

var readdir = q.denodeify(fs.readdir);
var fs_stat = q.denodeify(fs.stat);

var filterOut = function(data){
	

	for (var i = 0; i < data.length; i++) {
    	

    	if (!data[i].keep) {
        	data.splice(i--, 1);
    	}else{
    		delete data[i].parent;
    		delete data[i].keep;
    		filterOut(data[i].items);
    	}

	}



}

var processDir = function(path,parent){


	

	return readdir(path)
	.then(function(files){

		files.sort();


		var queue = [];
		files.forEach(function(v, i, a){
			var current_path = path + '/' + v;
		

			queue.push(
				fs_stat(current_path)
				.then(function(stat){
					if (stat.isDirectory()){
						var d = {
							name: getName(v),
							items: [],
							parent: parent,
						
						}

						parent.items.push(d);
						

						return processDir(current_path, d);
					}

			// retain only has pdf
					if (v.toLowerCase().lastIndexOf('.pdf') > 0){

						var p = parent;
						while(1){

							if (p.keep){
								break;
							}
							if (!p.parent){
								break;
							}


							p.keep = true;

							p = p.parent;

						}

						parent.file = current_path;

						if (parent.name.toLowerCase() == 'materi'){
							parent['type'] = 'book';	
							parent.parent['type'] = 'package';

						}

					}

					if (v.toLowerCase() === 'kunci.txt'){

						parent.key = current_path;
						parent['type'] = 'top';
						parent.parent['type'] = 'package';

					}



				})
			);

		});

		
		return q.all(queue);

	});

	
			
}


var readTree = function(callback){

	
}

var getTree = function(callback){
	

	var treeFile = __dirname + "/tree.json";

	fs.readFile(treeFile, function(err, data){


		if (err){
			var tree = {

				name: 'root',
				items: []

			};

			processDir(base_path,tree).then(function(){
				filterOut(tree.items);
				//write to file 

				fs.writeFile(treeFile, JSON.stringify(tree),function (err) {
		  			if (err) throw err;
		  			console.log('It\'s saved!');
				});
				callback(tree);
			});

		}else{
			console.log('read from file');

			var d = JSON.parse(data);
			callback(d);
		}

	});





}

module.exports.getTree = getTree;
